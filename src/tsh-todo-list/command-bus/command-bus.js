class CommandBus {
  constructor(handlers) {
    this.handlers = handlers;
  }

  execute(command) {
    const handler = this.handlers.find(handler => handler.supports(command));

    if (!handler) {
      return Promise.reject(`Command: ${command.type} is not supported.`);
    }

    return handler.execute(command);
  }
}

module.exports = CommandBus;