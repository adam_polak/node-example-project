const { HttpError } = require('./http.error');

class ValidationError extends HttpError {
  constructor(errors) {
    super('Validation errors', 400);

    this.errors = errors;
  }
}

module.exports = { ValidationError };
