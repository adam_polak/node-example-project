const awilix = require('awilix');
const { createConnection } = require('typeorm');
const Server = require('./server');
const { createRouting } = require('../routes/routing');
const { createTodosRouting } = require('../routes/todos/todos.routing');
const createTodosActions = require('../routes/todos/actions');
const createCommandBus = require('./command-bus');
const config = require('./config');
const { Todo } = require('../routes/todos/model/todo/todo')

module.exports = async () => {
  const container = awilix.createContainer();

  const dbConnection = await createConnection(config.db)

  container.register({
    server: awilix.asClass(Server).singleton(),
    todosActions: awilix.asFunction(createTodosActions).singleton(),
    todosRepository: awilix.asValue(await dbConnection.getRepository(Todo)),
    commandBus: awilix.asFunction(createCommandBus).singleton(),
    routing: awilix.asFunction(createRouting).singleton(),
    todosRouting: awilix.asFunction(createTodosRouting).singleton(),
    dbConnection: awilix.asValue(dbConnection)
  });

  return container;
}