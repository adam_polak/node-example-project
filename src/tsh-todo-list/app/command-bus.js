const CommandBus = require('../command-bus/command-bus');
const CreateTodoHandler = require('../routes/todos/handlers/create-todo.handler');
const UpdateTodoHandler = require('../routes/todos/handlers/update-todo.handler');
const RemoveTodoHandler = require('../routes/todos/handlers/remove-todo.handler');

module.exports = ({ todosRepository}) => new CommandBus([
  new CreateTodoHandler(todosRepository),
  new UpdateTodoHandler(todosRepository),
  new RemoveTodoHandler(todosRepository)
]);