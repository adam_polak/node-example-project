const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const config = require('./config');
const { errors, isCelebrate } = require('celebrate')
const { NotFoundError } = require('../errors/not-found.error');
const { ValidationError } = require('../errors/validation.error');
const { handleErrors } = require('./error.handler')

class Server {
  constructor({ routing }) {
    this.app = express();

    this.app.use(morgan('dev'));
    this.app.use(helmet());
    this.app.use(cors());
    this.app.use(express.json());

    this.app.use(routing);

    this.app.use('*', (req, res, next) => {
      next(new NotFoundError('Page not found'))
    })
    // celebrate default error handling
    // this.app.use(errors());

    this.app.use((error, req, res, next) => {
      if (isCelebrate(error)) {
        next(
          new ValidationError(error.details)
        );
        return;
      }

      next(error);
    });
    this.app.use(handleErrors);
  }

  getApp() {
    return this.app;
  }

  start() {
    this.app.listen(config.port)
  }
}

module.exports = Server;