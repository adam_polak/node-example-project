const { AppError } = require('../errors/app.error');
const { ValidationError } = require('../errors/validation.error');
const { HttpError } = require('../errors/http.error');
const { logger } = require('./logger')

const handleErrors = (error, req, res, next) => {
  logger.error(error)

  if (error instanceof ValidationError) {
    res
      .status(error.status)
      .json({
        code: error.status,
        message: error.message,
        errors: error.errors
      });
      
    return;
  }

  if (error instanceof HttpError) {
    res
      .status(error.status)
      .json({
        code: error.status,
        message: error.message
      });

    return;
  }

  if (error instanceof AppError) {
    res
      .status(500)
      .json({
        code: 500,
        message: error.message
      });

    return;
  }

  res
    .status(500)
    .send('Unknown error')
}

module.exports = {
  handleErrors
}