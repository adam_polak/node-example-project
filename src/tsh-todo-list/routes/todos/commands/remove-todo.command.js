class RemoveTodoCommand {
  constructor(id) {
    this.id = id;
    this.type = RemoveTodoCommand.TYPE
  }
}

RemoveTodoCommand.TYPE = 'RemoveTodo';

module.exports = RemoveTodoCommand;
