class UpdateTodoCommand {
  constructor(id, name, done) {
    this.id = id;
    this.name = name;
    this.done = done;
    this.type = UpdateTodoCommand.TYPE
  }
}

UpdateTodoCommand.TYPE = 'UpdateTodo';

module.exports = UpdateTodoCommand;
