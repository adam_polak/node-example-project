const RemoveTodoCommand = require('../commands/remove-todo.command');
const { AppError } = require('../../../errors/app.error');

class RemoveTaskHandler {
  constructor(todosRepository) {
    this.todosRepository = todosRepository;
  }

  supports(command) {
    return command.type === RemoveTodoCommand.TYPE
  }

  async execute(command) {
    await this.todosRepository
      .findOne(command.id)
      .then(todo => this.todosRepository.remove(todo))
      .catch(() => {
        throw new AppError("Could not remove task")
      })
  }
}

module.exports = RemoveTaskHandler