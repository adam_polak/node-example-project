const UpdateTodoCommand = require('../commands/update-todo.command');
const { AppError } = require('../../../errors/app.error');

class UpdateTaskHandler {
  constructor(todosRepository) {
    this.todosRepository = todosRepository;
  }

  supports(command) {
    return command.type === UpdateTodoCommand.TYPE
  }

  async execute(command) {
    const todo = await this.todosRepository.findOne(command.id);

    todo.update({
      name: command.name,
      done: command.done
    })

    await this.todosRepository
      .save(todo)
      .catch(() => {
        throw new AppError("Could not update task")
      })
  }
}

module.exports = UpdateTaskHandler