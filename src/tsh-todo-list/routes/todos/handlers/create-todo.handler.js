const CreateTodoCommand = require('../commands/create-todo.command');
const { Todo } = require('../model/todo/todo');
const { AppError } = require('../../../errors/app.error');

class CreateTaskHandler {
  constructor(todosRepository) {
    this.todosRepository = todosRepository;
  }

  supports(command) {
    return command.type === CreateTodoCommand.TYPE
  }

  async execute(command) {
    const todo = new Todo(command.id, command.name);

    await this.todosRepository
      .insert(todo)
      .catch(() => {
        throw new AppError("Could not create new task")
      })
  }
}

module.exports = CreateTaskHandler