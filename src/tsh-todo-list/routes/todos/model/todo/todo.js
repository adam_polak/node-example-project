class Todo {
  constructor(id, name, done = false) {
    this.id = id;
    this.name = name;
    this.done = done;
  }

  update(updatedData) {
    this.name = updatedData.name;
    this.done = updatedData.done;
  }
}

module.exports = {
  Todo: Todo
}