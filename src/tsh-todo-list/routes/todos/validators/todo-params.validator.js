const { Joi } = require('celebrate');

module.exports = Joi.object().keys({
  id: Joi.string().uuid().required()
});