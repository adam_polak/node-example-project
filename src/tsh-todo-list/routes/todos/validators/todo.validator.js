const { Joi } = require('celebrate');

module.exports = {
  newTodo: Joi.object().keys({
    name: Joi.string().required(),
  }),
  updateTodo: Joi.object().keys({
    name: Joi.string().required(),
    done: Joi.boolean().required()
  })
}