const todoValidator = require('./todo.validator');
const todoParamsValidator = require('./todo-params.validator');

module.exports = {
  todoValidator,
  todoParamsValidator
}