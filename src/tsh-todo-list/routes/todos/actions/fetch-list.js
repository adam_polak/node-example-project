module.exports = (todosRepository) => async (req, res) => {
  const todos = await todosRepository.find();

  return res
    .status(200)
    .json(todos);
}