const createContainer = require('../../../app/container');
const request = require('supertest');
const assert = require('assert');
const { Joi } = require('celebrate');

describe('Create todo', () => {
  it('returns validations errors', async () => {
    const container = await createContainer();
    const server = container.resolve('server');

    return request(server.getApp())
      .post('/todos')
      .expect(400)
      .then(response => {
        const expectedResponse = {
            "code":400,
            "message":
            "Validation errors",
            "errors":[
              {
                "message":"\"name\" is required",
                "path":["name"],
                "type":"any.required",
                "context":{
                  "key":"name",
                  "label":"name"
              }
            }
          ]
        };

        assert.deepEqual(response.body, expectedResponse);
      })
      .finally(() => {
        container.resolve('dbConnection').close();
      })
  })

  it('creates new todo', async () => {
    const container = await createContainer();
    const server = container.resolve('server');

    return request(server.getApp())
      .post('/todos')
      .send({
        name: 'My newest todo'
      })
      .expect(201)
      .then(response => {
        const expectedSchema = Joi.object().keys({
          id: Joi.string().uuid().required(),
          name: Joi.valid('My newest todo').required(),
          done: Joi.valid(false).required()
        });

        assert.equal(Joi.validate(response.body, expectedSchema).erros, null);
      })
      .finally(() => {
        container.resolve('dbConnection').close();
      })
  })
});