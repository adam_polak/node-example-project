const fetch = require('./fetch');
const fetchList = require('./fetch-list');
const create = require('./create');
const update = require('./update');
const remove = require('./remove');

module.exports = ({ commandBus, todosRepository }) => ({
  fetch: fetch(todosRepository),
  fetchList: fetchList(todosRepository),
  create: create(commandBus, todosRepository),
  update: update(commandBus, todosRepository),
  remove: remove(commandBus)
})