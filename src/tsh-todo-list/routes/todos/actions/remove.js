const RemoveTodoCommand = require('../commands/remove-todo.command');

module.exports = commandBus => async (req, res,next) => {
  const command = new RemoveTodoCommand(req.params.id);

  commandBus.execute(command)
    .then(async () => {
      res
        .status(204)
        .json()
    })
    .catch(next);
}