const UpdateTodoCommand = require('../commands/update-todo.command');
const { NotFoundError } = require('../../../errors/not-found.error');

module.exports = (commandBus, todosRepository) => async (req, res,next) => {
  const command = new UpdateTodoCommand(req.params.id, req.body.name, req.body.done);

  commandBus.execute(command)
    .then(async () => {
      const task = await todosRepository.findOne(req.params.id);

      if (!task) {
        next(new NotFoundError(`Todo with id: ${req.params.id} was not found`));
        return;
      }

      res
        .status(200)
        .json(task)
    })
    .catch(next);
}