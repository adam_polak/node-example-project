const CreateTodoCommand = require('../commands/create-todo.command');
const { v4 } = require('uuid');

module.exports = (commandBus, todosRepository) => async (req, res,next) => {
  const taskId = v4();
  const command = new CreateTodoCommand(taskId, req.body.name);

  commandBus.execute(command)
    .then(async () => {
      const task = await todosRepository.findOne(taskId);

      res
        .status(201)
        .json(task)
    })
    .catch(next);
}