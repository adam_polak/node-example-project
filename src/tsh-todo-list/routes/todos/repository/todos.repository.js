class TodosRepository {
  constructor() {
    this.todos = []
  }

  async add(todo) {
    this.todos.push(todo)
    return
  }

  async findOne(id) {
    const todo = this.todos.find(todo => todo.id === id);

    return Promise.resolve(todo || null);
  }

  async find() {
    return Promise.resolve(this.todos);
  }

  async remove(id) {
    if (!this.find(id)) {
      return Promise.reject();
    }

    this.todos = this.todos.filter(todo => todo.id !== id);

    return Promise.resolve();
  }

  async update(id, todo) {
    if (!this.find(id)) {
      return Promise.reject();
    }

    await this.remove(id);
    await this.add(todo)
    
    return Promise.resolve();
  }
}

module.exports = TodosRepository;