const express = require('express');
const { todoValidator, todoParamsValidator } = require('./validators')
const { celebrate } = require('celebrate');

const createTodosRouting = ({ todosActions }) => {
  const router = new express.Router();

  router.post('/',[celebrate({
    body: todoValidator.newTodo
  })], todosActions.create);
  router.get('/:id',[celebrate({
    params: todoParamsValidator
  })], todosActions.fetch);
  router.get('/', todosActions.fetchList);
  router.delete('/:id', [celebrate({
    params: todoParamsValidator
  })], todosActions.remove);
  router.put('/:id', [celebrate({
    body: todoValidator.updateTodo,
    params: todoParamsValidator
  })], todosActions.update);
  
  return router;
}

module.exports = {
  createTodosRouting
}